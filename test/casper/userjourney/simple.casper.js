describe('Home page', function() {
    before(function() {
        casper.start('http://localhost:3000');
    });
    it('should have an element in DOM', function(){
        casper.waitForSelector('.container-fluid', function() {
            expect('.container-fluid').to.be.inDOM;
        });
    });
});