import app from '../../browserapp';
import createMovieFormHTML from './createmovieform.html';
import createMovieFormCtrl from './createmovieformctrl';

app.directive('createMovieForm', [function(){
    return {
        template: createMovieFormHTML,
        controller: createMovieFormCtrl,
        controllerAs: 'formCtrl'
    };
}]);