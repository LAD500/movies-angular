import angular from 'angular';

import '../../services/movieslisting/';
import createMovieFormCtrl from './createmovieformctrl';

import createMovieMock from '../../utils/testing/mockmovie';

import {expect} from 'chai';

describe(createMovieFormCtrl, ()=>{
    let createCtrl, scope, moviesListing;

    beforeEach(angular.mock.module('BrowserApp'));

    beforeEach(angular.mock.inject(($rootScope, $controller)=>{
        moviesListing = {
            movieCollection: [],
            addMovie(movie){
                this.movieCollection.push(movie);
            }
        }
        scope = $rootScope.$new();
        createCtrl = ()=>{
            return $controller(createMovieFormCtrl, {
                $scope: scope,
                moviesListing: moviesListing
            });
        };
    }));

    it('should update subcategories to possible options when category has been selected', ()=>{
        createCtrl();

        expect(scope.subcategories).to.be.an('array');

        scope.categories = {
            option1 : ['subcat1 of cat1', 'subcat2 of cat1'],
            option2 : ['subcat1 of cat2', 'subcat2 of cat2']
        };

        scope.movie.category = 'option2';
        scope.categoryUpdated();

        expect(scope.subcategories).to.deep.equal(scope.categories.option2);

    });
    it('should not allow subcategory to be updated of there is no category selected', ()=>{
        createCtrl();

        expect(scope.subcategories).to.be.an('array');

        scope.categories = {
            option1 : ['subcat1 of cat1', 'subcat2 of cat1'],
            option2 : ['subcat1 of cat2', 'subcat2 of cat2']
        };

        scope.categoryUpdated();

        expect(scope.subcategories).to.deep.equal([]);
    });
    it('should allow deletion of actor when more than one actor is present in collection', ()=>{
        createCtrl();

        scope.movie = createMovieMock({ actors:[ { name:'john', salary: 10000 }, { name:'ian', salary: 20000 }]});
        
        scope.deleteActor(0);

        expect(scope.movie.actors).to.deep.equal([
            {
                name:'ian',
                salary: 20000
            }
        ]);
    });
    it('should not allow deletion of actor if only one actor is present in collection', ()=>{
        createCtrl();

        scope.movie = createMovieMock({ actors:[ { name:'john', salary: 10000 }]});

        scope.deleteActor(0);

        expect(scope.movie.actors).to.deep.equal([
            {
                name:'john',
                salary: 10000
            }
        ]);
    });
    it('should allow an actor to be initialised and added to the collection if all actors are valid', ()=>{
        createCtrl();

        scope.movie = createMovieMock({ actors:[ { name:'john', salary: 10000 }]});

        scope.initialiseNewActor();

        expect(scope.movie.actors).to.deep.equal([
            {
                name:'john',
                salary: 10000
            },
            {
                name:'',
                salary: 0
            }
        ]);
    });
    it('should not allow an actor to be initialised and added to the collection if current actors are invalid', ()=>{
        createCtrl();

        scope.movie = createMovieMock({ actors:[ { name:'', salary: 0 }]});


        expect(scope.movie.actors).to.deep.equal([
            {
                name:'',
                salary: 0
            }
        ]);

        scope.initialiseNewActor();
        expect(scope.movie.actors).to.deep.equal([
            {
                name:'',
                salary: 0
            }
        ]);
    });
    it('should inform if all actors are valid', ()=>{
        createCtrl();

        scope.movie = createMovieMock({ actors:[ { name:'', salary: 0 }]});
        expect(scope.allActorsAreValid()).to.be.false;

        scope.movie = createMovieMock({ actors:[ { name:'', salary: 10000 }]});
        expect(scope.allActorsAreValid()).to.be.false;

        scope.movie = createMovieMock({ actors:[ { name:'a name', salary: 10000 }]});
        expect(scope.allActorsAreValid()).to.be.true;

        scope.movie = createMovieMock({ actors:[ { name:'some name', salary: 0 }]});
        expect(scope.allActorsAreValid()).to.be.false;

        scope.movie = createMovieMock({ actors:[ { name:'some name', salary: 1000 }, { name:'', salary: 1000 }]});
        expect(scope.allActorsAreValid()).to.be.false;

        scope.movie = createMovieMock({ actors:[ { name:'some name', salary: 1000 }, { name:'another name', salary: 1000 }]});
        expect(scope.allActorsAreValid()).to.be.true;

    });
    it('should inform if has at least one valid actor', ()=>{
        createCtrl();

        scope.movie = createMovieMock({ actors:[ { name:'', salary: 0 }]});
        expect(scope.hasOneValidActor()).to.be.false;

        scope.movie = createMovieMock({ actors:[ { name:'a name', salary: 0 }]});
        expect(scope.hasOneValidActor()).to.be.false;

        scope.movie = createMovieMock({ actors:[ { name:'', salary: 1000 }]});
        expect(scope.hasOneValidActor()).to.be.false;

        scope.movie = createMovieMock({ actors:[ { name:'a name', salary: 1000 }]});
        expect(scope.hasOneValidActor()).to.be.true;
    });
    it('should allow the addiiton of a movie to the collection if form is valid',()=>{
        createCtrl();

        scope.movie = createMovieMock();

        scope.create({ $valid: true });

        expect(moviesListing.movieCollection[0]).to.deep.equal({
            title:'Some title',
            category:'Some category',
            actors:[{
                name:'some actor',
                salary:10000
            }]
        });
    });
    it('should not allow the additon of a movie to the collection if form is invalid',()=>{
        createCtrl();

        scope.movie = createMovieMock();

        scope.create({ $valid: false });

        expect(moviesListing.movieCollection.length).to.equal(0);
    });
    it('should provide the means to reset the form',()=>{
        createCtrl();

        scope.movie = createMovieMock();

        let mockForm = {
            $setPristine(){
                this.setPristineCalled = true;
            },
            $setUntouched(){
                this.setUntouchedCalled = true;
            }
        };

        scope.resetForm(mockForm);

        expect(scope.movie).to.deep.equal({
            title: '',
            category: '',
            actors: [
                {
                    name: '',
                    salary: 0
                }
            ]
        });
        expect(mockForm.setPristineCalled).to.be.true;
        expect(mockForm.setUntouchedCalled).to.be.true;
    });
});

