import app from '../../browserapp';
import angular from 'angular';
import trim from 'trim';

import categoryOptions from './moviecategoryoptions';

let controllerName = 'CreateMovieFormCtrl';

app.controller(controllerName, ['$scope', 'moviesListing', ($scope, moviesListing)=>{

    $scope.movie = {
        title: '',
        category: '',
        actors: [
            {
                name: '',
                salary: 0
            }
        ]
    };
    
    $scope.categories = categoryOptions;
    $scope.subcategories = [];
    
    $scope.categoryUpdated = ()=>{
        if($scope.movie.category){
            $scope.subcategories = $scope.categories[$scope.movie.category];
        }
    };

    $scope.deleteActor = (index)=>{
        if($scope.movie.actors.length > 1) {
            $scope.movie.actors.splice(index, 1);
        }
    };

    $scope.initialiseNewActor = () => {
        if($scope.allActorsAreValid()){
            $scope.movie.actors.push({
                name:'',
                salary: 0
            });
        }
    };

    $scope.allActorsAreValid = ()=>{
        let valid = true;
        
        for(let i=0, len=$scope.movie.actors.length, actor; i < len; i++){
            actor = $scope.movie.actors[i];
            if(trim(actor.name).length === 0 || !actor.salary){
                valid = false;
                break;
            }
        }
        return valid;
    };

    $scope.hasOneValidActor = ()=>{
        let firstActor = $scope.movie.actors[0];
        return trim(firstActor.name).length > 0 && !!firstActor.salary;
    };

    $scope.resetForm = (createMovieForm)=>{
        $scope.movie = {
            title: '',
            category: '',
            actors: [
                {
                    name: '',
                    salary: 0
                }
            ]
        };

        createMovieForm.$setPristine();
        createMovieForm.$setUntouched();
    };

    $scope.create = (createMovieForm)=>{
        if(createMovieForm.$valid) {
            let movie = angular.copy($scope.movie);
            moviesListing.addMovie(movie);
        }
    };

}]);

export default controllerName;
