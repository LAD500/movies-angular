import app from '../../browserapp';
import moviesFormHTML from './movieslist.html';
import moviesFormCtrl from './movieslistctrl';

app.directive('moviesList', [function(){
    return {
        template: moviesFormHTML,
        controller: moviesFormCtrl,
        controllerAs: 'listCtrl'
    };
}]);