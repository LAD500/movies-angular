import app from '../../browserapp';

let controllerName = 'MoviesListCtrl';

app.controller('MoviesListCtrl', ['$scope', 'moviesListing', function($scope, moviesListing) {

    $scope.moviesList = {
        moviesFilter: 'costLowToHigh',
        movies: moviesListing.movieCollection
    };

    $scope.createMovieHeader = movie =>{
        let prefix = `${movie.title} (${movie.category}`;
        return movie.subcategory ? `${prefix}/${movie.subcategory})` : `${prefix})`;
    };

    $scope.updateFilter = ()=>{
        moviesListing.sortMovies($scope.moviesList.moviesFilter);
    };
    
}]);

export default controllerName;
