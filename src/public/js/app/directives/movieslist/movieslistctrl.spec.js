import angular from 'angular';

import '../../services/movieslisting/';
import moviesListCtrl from './movieslistctrl';

import createMovieMock from '../../utils/testing/mockmovie';

import {expect} from 'chai';

describe(moviesListCtrl, ()=>{
    let createCtrl, scope, moviesListing;

    beforeEach(angular.mock.module('BrowserApp'));

    beforeEach(angular.mock.inject(($rootScope, $controller)=>{
        moviesListing = {
            sortMovies(filterKey){
                this.passedFilter = filterKey
            }
        };
        scope = $rootScope.$new();
        createCtrl = ()=>{
            return $controller(moviesListCtrl, {
                $scope: scope,
                moviesListing: moviesListing
            });
        };
    }));

    it('should return the Movie header correctly when only given a category', ()=>{
        createCtrl();
        let movieHeader = scope.createMovieHeader(createMovieMock());
        expect(movieHeader).to.equal('Some title (Some category)');
    });
    it('should return the Movie header correctly when given a category and a subcategory', ()=>{
        createCtrl();
        let movieHeader = scope.createMovieHeader(createMovieMock({subcategory:'Subcategory here'}));
        expect(movieHeader).to.equal('Some title (Some category/Subcategory here)');
    });
    it('should pass the sort label to the movieslisting service', ()=>{
        createCtrl();
        scope.moviesList.moviesFilter = 'some filter value';
        scope.updateFilter();
        expect(moviesListing.passedFilter).to.equal('some filter value');
    });
});

