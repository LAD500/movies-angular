import app from '../../browserapp';

let factoryName = 'moviesListing';

app.factory(factoryName, [function() {
    let movieCollection = [];

    let currentSortKey = 'costLowToHigh';

    let costLowToHigh = (a, b)=>{
        if (a.castListTotalCost > b.castListTotalCost) {
            return 1;
        }
        if (a.castListTotalCost < b.castListTotalCost) {
            return -1;
        }
        return 0;
    };
    let costHighToLow = (a, b)=>{
        if (a.castListTotalCost < b.castListTotalCost) {
            return 1;
        }
        if (a.castListTotalCost > b.castListTotalCost) {
            return -1;
        }
        return 0;
    };

    let sortFunctionsLookup = {
        costLowToHigh,
        costHighToLow
    };

    let castListTotalCost = movie => movie.actors.reduce((total, actor) => total +  actor.salary, 0);

    let addMovie = (movie)=>{
        movie.castListTotalCost = castListTotalCost(movie);
        movieCollection.push(movie);
        movieCollection.sort(sortFunctionsLookup[currentSortKey]);
    };

    let sortMovies = (sortKey)=>{
        currentSortKey = sortKey;
        movieCollection.sort(sortFunctionsLookup[currentSortKey]);
    };

    let reset = ()=>{
        movieCollection.length = 0;
    };

    return {
        movieCollection,
        addMovie,
        sortMovies,
        reset
    };
}]);

export default factoryName;