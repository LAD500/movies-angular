import angular from 'angular';
import './index';
import '../../browserapp.js';

import createMovieMock from '../../utils/testing/mockmovie';

import {expect} from 'chai';

let movieslisting;

beforeEach(()=>{
    movieslisting = angular.injector(['ng', 'BrowserApp']).get('moviesListing');
    movieslisting.reset();
});

describe('movieslisting', ()=>{
    it('should add a movie to the collection when addMovie is called adding a casting total cost property', ()=>{
        expect(movieslisting.movieCollection.length).to.equal(0);

        movieslisting.addMovie(createMovieMock({ actors:[ { name:'some name', salary: 20000 }, { name:'another name', salary: 1000 }]}));

        expect(movieslisting.movieCollection.length).to.equal(1);
        expect(movieslisting.movieCollection[0].castListTotalCost).to.equal(21000);
    });

    it('should sort movies in the correct order when asked to display order highest cost to lowest', ()=>{
        expect(movieslisting.movieCollection.length).to.equal(0);

        movieslisting.addMovie(createMovieMock({title:'lowest'}, {salary:10}));
        movieslisting.addMovie(createMovieMock({title:'mid'}, {salary:100}));
        movieslisting.addMovie(createMovieMock({title:'highest'}, {salary:1000}));

        expect(movieslisting.movieCollection[0].title).to.equal('lowest');
        expect(movieslisting.movieCollection[0].actors[0].salary).to.equal(10);

        movieslisting.sortMovies('costHighToLow');

        expect(movieslisting.movieCollection[0].title).to.equal('highest');
        expect(movieslisting.movieCollection[0].actors[0].salary).to.equal(1000);

        expect(movieslisting.movieCollection[1].title).to.equal('mid');
        expect(movieslisting.movieCollection[1].actors[0].salary).to.equal(100);

        expect(movieslisting.movieCollection[2].title).to.equal('lowest');
        expect(movieslisting.movieCollection[2].actors[0].salary).to.equal(10);
    });

    it('should sort movies in the correct order when asked to display order highest cost to lowest', ()=>{
        expect(movieslisting.movieCollection.length).to.equal(0);

        movieslisting.sortMovies('costHighToLow');

        movieslisting.addMovie(createMovieMock({title:'highest'}, {salary:1000}));
        movieslisting.addMovie(createMovieMock({title:'mid'}, {salary:100}));
        movieslisting.addMovie(createMovieMock({title:'lowest'}, {salary:10}));

        expect(movieslisting.movieCollection[0].title).to.equal('highest');
        expect(movieslisting.movieCollection[0].actors[0].salary).to.equal(1000);

        movieslisting.sortMovies('costLowToHigh');

        expect(movieslisting.movieCollection[0].title).to.equal('lowest');
        expect(movieslisting.movieCollection[0].actors[0].salary).to.equal(10);

        expect(movieslisting.movieCollection[1].title).to.equal('mid');
        expect(movieslisting.movieCollection[1].actors[0].salary).to.equal(100);

        expect(movieslisting.movieCollection[2].title).to.equal('highest');
        expect(movieslisting.movieCollection[2].actors[0].salary).to.equal(1000);
    });
});

