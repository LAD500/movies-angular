import * as angular from 'angular';

var app = angular.module('BrowserApp', []);

app.init = ()=>{
    angular.element(document).ready(function() {
        angular.bootstrap(document, ['BrowserApp']);
    });
};

export default app;
