let createMovieMock = (movie = {}, actor={}) => {
    let mockActor = {
        name:'some actor',
        salary: 10000
    };

    let movieMovie = {
        title: 'Some title',
        category: 'Some category',
        actors: [ Object.assign(mockActor, actor) ]
    };

    return Object.assign(movieMovie, movie);
};

export default createMovieMock;