import 'es5-shim';
import 'es6-shim';

import './app/services/movieslisting';
import './app/directives/createmovieform';
import './app/directives/movieslist';

import app from './app/browserapp';

app.init();

